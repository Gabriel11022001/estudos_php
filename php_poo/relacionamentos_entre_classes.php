<?php

require_once './src/classes/relacionamentos/Email.php';
require_once './src/classes/relacionamentos/Pessoa.php';

try {
    $pessoa = new Pessoa(
        'Gabriel',
        21,
        'Masculino',
        87.76,
        1.83
    );
    $pessoa->adicionarEmail(
        new Email('gabriel@gmail.com.br')
    );
    $pessoa->adicionarEmail(new Email('gabriel_santos@gmail.com'));
    $pessoa->adicionarEmail(new Email('meu_email@gmail.com'));
    echo '<pre>';
    print_r($pessoa->getEmails());
    echo '</pre>';
    $pessoa->apresentarDados();
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}