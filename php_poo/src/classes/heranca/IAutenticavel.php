<?php

interface IAutenticavel
{
    function login(string $login, string $senha): bool;
}