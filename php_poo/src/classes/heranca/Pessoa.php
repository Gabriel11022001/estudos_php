<?php

/**
 * Essa classe Pessoa é uma classe abstrata,
 * ou seja, eu não posso criar objetos
 * a partir dessa classe, somente das classes concretas
 * que herdarem da mesma.
 */
abstract class Pessoa implements IAutenticavel
{
    /**
     * Como todos os atributos possuem
     * o nível de acesso private, eu não posso
     * acessar essas propriedades diretamente do objeto,
     * somente no escopo da própria classe.
     */
    private string $nome;
    private int $idade;
    private string $sexo;
    private float $peso;
    private float $altura;
    private array $emails;
    private string $login;
    private string $senha;

    /**
     * Método construtor
     */
    public function __construct(
        string $nome,
        int $idade,
        string $sexo,
        float $peso,
        float $altura,
        string $login,
        string $senha
    )
    {
        $this->setNome($nome);
        $this->setIdade($idade);
        $this->setSexo($sexo);
        $this->setPeso($peso);
        $this->setAltura($altura);
        $this->setLogin($login);
        $this->setSenha($senha);
        $this->emails = [];
    }
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }
    public function getNome(): string
    {
        return $this->nome;
    }
    public function setIdade(int $idade): void
    {
        if ($idade < 0 || $idade >= 120) {
            throw new InvalidArgumentException('Idade não permitida!');
        }
        $this->idade = $idade;
    }
    public function getIdade(): int
    {
        return $this->idade;
    }
    public function setSexo(string $sexo): void
    {
        $this->sexo = $sexo;
    }
    public function getSexo(): string
    {
        return $this->sexo;
    }
    public function setPeso(float $peso): void
    {
        $this->peso = $peso;
    }
    public function getPeso(): float
    {
        return $this->peso;
    }
    public function setAltura(float $altura): void
    {
        $this->altura = $altura;
    }
    public function getAltura(): float
    {
        return $this->altura;
    }
    public function apresentarDados(): void
    {
        echo 'Nome: ' . $this->getNome() . '<br>';
        echo 'Idade: ' . $this->getIdade() . '<br>';
        echo 'Sexo: ' . $this->getSexo() . '<br>';
        echo 'Peso: ' . $this->getPeso() . 'kg<br>';
        echo 'Altura: ' . $this->getAltura() . 'm<br>';
        echo 'Login: ' . $this->getLogin() . '<br>';
        echo 'Senha: ' . $this->getSenha() . '<br>';
        echo 'E-mails:<br>';
        if (count($this->getEmails()) == 0) {
            echo 'Não possui e-mail!<br>';
            return;
        }
        foreach ($this->getEmails() as $email) {
            echo $email->getEmail() . '<br>';
        }
    }
    public function adicionarEmail(Email $email): void
    {
        $this->emails[] = $email;
    }
    public function getEmails(): array
    {
        return $this->emails;
    }
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }
    public function getLogin(): string
    {
        return $this->login;
    }
    public function setSenha(string $senha): void
    {
        $this->senha = $senha;
    }
    public function getSenha(): string
    {
        return $this->senha;
    }
}