<?php

/**
 * A classe Funcionario herda da classe Pessoa
 */
class Funcionario extends Pessoa
{
    private string $cargo;
    private float $salario;

    public function __construct(
        string $nome,
        int $idade,
        string $sexo,
        float $peso,
        float $altura,
        string $login,
        string $senha,
        string $cargo,
        float $salario
    )
    {
        parent::__construct(
            $nome,
            $idade,
            $sexo,
            $peso,
            $altura,
            $login,
            $senha
        );
        $this->setCargo($cargo);
        $this->setSalario($salario);
    }
    public function setCargo(string $cargo): void
    {
        $this->cargo = $cargo;
    }
    public function getCargo(): string
    {
        return $this->cargo;
    }
    public function setSalario(float $salario): void
    {
        if ($salario < 0) {
            throw new InvalidArgumentException('Salário inválido!');
        }
        $this->salario = $salario;
    }
    public function getSalario(): float
    {
        return $this->salario;
    }
    public function apresentarDados(): void
    {
        parent::apresentarDados();
        echo 'Cargo do funcionário: ' . $this->getCargo() . '<br>';
        echo 'Salário do funcionário: R$' . $this->getSalario() . '<br>';
    }
    public function login(string $login, string $senha): bool
    {
        if ($login === $this->getLogin()
        && $senha === $this->getSenha()) {
            return true;
        }
        return false;
    }
}