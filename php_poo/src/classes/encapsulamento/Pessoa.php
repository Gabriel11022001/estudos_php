<?php

class Pessoa
{
    /**
     * Como todos os atributos possuem
     * o nível de acesso private, eu não posso
     * acessar essas propriedades diretamente do objeto,
     * somente no escopo da própria classe.
     */
    private string $nome;
    private int $idade;
    private string $sexo;
    private float $peso;
    private float $altura;

    /**
     * Método construtor
     */
    public function __construct(
        string $nome,
        int $idade,
        string $sexo,
        float $peso,
        float $altura
    )
    {
        $this->setNome($nome);
        $this->setIdade($idade);
        $this->setSexo($sexo);
        $this->setPeso($peso);
        $this->setAltura($altura);
    }
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }
    public function getNome(): string
    {
        return $this->nome;
    }
    public function setIdade(int $idade): void
    {
        if ($idade < 0 || $idade >= 120) {
            throw new InvalidArgumentException('Idade não permitida!');
        }
        $this->idade = $idade;
    }
    public function getIdade(): int
    {
        return $this->idade;
    }
    public function setSexo(string $sexo): void
    {
        $this->sexo = $sexo;
    }
    public function getSexo(): string
    {
        return $this->sexo;
    }
    public function setPeso(float $peso): void
    {
        $this->peso = $peso;
    }
    public function getPeso(): float
    {
        return $this->peso;
    }
    public function setAltura(float $altura): void
    {
        $this->altura = $altura;
    }
    public function getAltura(): float
    {
        return $this->altura;
    }
    public function apresentarDados(): void
    {
        echo 'Nome: ' . $this->getNome() . '<br>';
        echo 'Idade: ' . $this->getIdade() . '<br>';
        echo 'Sexo: ' . $this->getSexo() . '<br>';
        echo 'Peso: ' . $this->getPeso() . 'kg<br>';
        echo 'Altura: ' . $this->getAltura() . 'm<br>';
    }
}