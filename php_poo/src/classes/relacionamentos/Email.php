<?php

class Email
{
    private string $email;

    public function __construct(string $email)
    {
        $this->setEmail($email);
    }
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function getEmail(): string
    {
        return $this->email;
    }
}