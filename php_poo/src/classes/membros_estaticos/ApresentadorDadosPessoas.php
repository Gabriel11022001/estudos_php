<?php

class ApresentadorDadosPessoas
{
    public static function apresentarDados(...$pessoas): void
    {
        if (count($pessoas) != 0) {
            foreach ($pessoas as $pessoa) {
                echo 'Dados de ' . $pessoa->getNome() . '<br>';
                $pessoa->apresentarDados();
            }
        } else {
            echo 'Não foram passadas como argumento pessoas!<br>';
        }
    }
}