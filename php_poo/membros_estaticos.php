<?php

require_once './src/classes/encapsulamento/Pessoa.php';
require_once './src/classes/membros_estaticos/ApresentadorDadosPessoas.php';

$pessoa1 = new Pessoa('Gabriel', 21, 'Masculnino', 87, 1.87);
$pessoa2 = new Pessoa('Maria', 21, 'Feminino', 65, 1.56);
ApresentadorDadosPessoas::apresentarDados($pessoa1, $pessoa2);