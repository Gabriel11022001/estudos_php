<?php

require_once './src/classes/encapsulamento/Pessoa.php';

try {
    $pessoa1 = new Pessoa(
        'Gabriel',
        21,
        'Masculino',
        87,
        1.87
    );
    echo '========== Apresentando os dados da pessoa 1: ==========<br>';
    $pessoa1->apresentarDados();
    $pessoa2 = new Pessoa(
        'Gustavo Pereira da Silva',
        22,
        'Masculino',
        89,
        1.89
    );
    echo '========== Apresentando os dados da pessoa 2: ==========<br>';
    $pessoa2->apresentarDados();
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}