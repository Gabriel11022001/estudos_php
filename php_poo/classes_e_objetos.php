<?php

require_once './src/classes/classes_e_objetos/Pessoa.php';

$pessoa1 = new Pessoa('Gabriel', 21, 'Masculino', 90, 1.87);
$pessoa2 = new Pessoa('Maria', 22, 'Feminino', 65, 1.76);
$pessoa1->nome = 'Gabriel Rodrigues dos Santos';
$pessoa1->idade = 21;
$pessoa1->sexo = 'Masculino';
$pessoa1->peso = 87.98;
$pessoa1->altura = 1.83;
$pessoa2->nome = 'Maria Fernanda da Silva';
$pessoa2->idade = 42;
$pessoa2->sexo = 'Feminino';
$pessoa2->peso = 67.87;
$pessoa2->altura = 1.76;
echo '<pre>';
var_dump($pessoa1, $pessoa2);
echo '</pre>';
echo '========== Apresentando os dados da pessoa 1: ==========<br>';
echo 'Nome da pessoa 1: ' . $pessoa1->nome . '<br>';
echo 'Idade da pessoa 1: ' . $pessoa1->idade . '<br>';
echo 'Sexo da pessoa 1: ' . $pessoa1->sexo . '<br>';
echo 'Peso da pessoa 1: ' . $pessoa1->peso . 'kg<br>';
echo 'Altura da pessoa 1: ' . $pessoa1->altura . 'm<br>';
echo '========== Apresentando os dados da pessoa 2: ==========<br>';
echo 'Nome da pessoa 2: ' . $pessoa2->nome . '<br>';
echo 'Idade da pessoa 2: ' . $pessoa2->idade . '<br>';
echo 'Sexo da pessoa 2: ' . $pessoa2->sexo . '<br>';
echo 'Peso da pessoa 2: ' . $pessoa2->peso . 'kg<br>';
echo 'Altura da pessoa 2: ' . $pessoa2->altura . 'm<br>';
$pessoa3 = new Pessoa(
    'Pedro',
    22,
    'Masculino',
    98,
    1.87
);
echo '========== Apresentando os dados da pesso 3: ==========<br>';
echo 'Nome da pessoa 3: ' . $pessoa3->getNome() . '<br>';
echo 'Idade da pessoa 3: ' . $pessoa3->getIdade() . '<br>';
echo 'Sexo da pessoa 3: ' . $pessoa3->getSexo() . '<br>';
echo 'Peso da pessoa 3: ' . $pessoa3->getPeso() . 'kg<br>';
echo 'Altura da pessoa 3: ' . $pessoa3->getAltura() . '<br>';
$pessoa3->setNome('Pedro Pereira da Silva');
echo 'Nome da pessoa 3 após alteração: ' . $pessoa3->getNome() . '<br>';