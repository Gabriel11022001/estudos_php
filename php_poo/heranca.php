<?php

require_once './src/classes/heranca/IAutenticavel.php';
require_once './src/classes/heranca/Email.php';
require_once './src/classes/heranca/Pessoa.php';
require_once './src/classes/heranca/Funcionario.php';

try {
    $funcionario1 = new Funcionario(
        'Gabriel',
        21,
        'Masculino',
        87,
        1.87,
        'gabriel123',
        'Gabriel132',
        'Analista de Sistemas',
        4000
    );
    $funcionario2 = new Funcionario(
        'Pedro',
        33,
        'Masculino',
        76,
        1.76,
        'pedro321',
        'Pedro123',
        'Analista de Sistemas',
        4000
    );
    $funcionario1->adicionarEmail(new Email('gabriel@gmail.com'));
    $funcionario2->adicionarEmail(new Email('pedro@gmail.com'));
    $funcionario1->apresentarDados();
    $funcionario2->apresentarDados();
    echo $funcionario1->login('gabriel123', 'Gabriel132') . '<br>';
    echo $funcionario2->login('pedro321', 'Pedro123') . '<br>';
    var_dump($funcionario1->login('gabriel122', 'Gabriel4343'));
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}