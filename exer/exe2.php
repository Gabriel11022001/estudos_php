<?php

require_once './sortear_array_numerico.php';

$numeros = $sortearNumeros(10);
echo '<pre>';
print_r($numeros);
echo '</pre>';
// inverter elementos.
$novosNumeros = [];
$ultimoIndice = count($numeros) - 1;
for ($contador = 0; $contador < count($numeros); $contador++) {
    $novosNumeros[$contador] = $numeros[$ultimoIndice];
    $ultimoIndice--;
}
echo '<pre>';
print_r($novosNumeros);
echo '</pre>';