<?php

$candidatos = [
    [
        'nome' => 'Gabriel',
        'idade' => 21,
        'sexo' => 'M',
        'experiencia_no_servico' => true
    ],
    [
        'nome' => 'Carlos',
        'idade' => 32,
        'sexo' => 'M',
        'experiencia_no_servico' => true
    ],
    [
        'nome' => 'Maria',
        'idade' => 43,
        'sexo' => 'F',
        'experiencia_no_servico' => false
    ],
    [
        'nome' => 'Eduarda',
        'idade' => 22,
        'sexo' => 'F',
        'experiencia_no_servico' => true
    ],
    [
        'nome' => 'Fernanda',
        'idade' => 33,
        'sexo' => 'F',
        'experiencia_no_servico' => false
    ]
];
$numeroCandidatosSexoFeminino = 0;
$numeroCandidatosSexoMasculino = 0;
$quantidadeHomensComExpecienciaNoServico = 0;
$somaIdadesHomensComExperienciaNoServico = 0;
$mediaIdadesHomensComExperienciaNoServico = 0;
$porcentagemDosHomensComMaisDe45AnosEntreOTotalDosHomens = 0;
$quantidadeDeHomensComMaisDe45Anos = 0;
$numeroDeMulheresComIdadeInferiorA35AnosComExpecienciaNoServico = 0;
$menorIdadeEntreAsMulheresQueJaTemExperienciaNoServico = 99999;

foreach ($candidatos as $candidato) {

    if ($candidato['sexo'] === 'M') {
        $numeroCandidatosSexoMasculino++;

        if ($candidato['experiencia_no_servico']) {
            $quantidadeHomensComExpecienciaNoServico++;
            $somaIdadesHomensComExperienciaNoServico += $candidato['idade'];
        }

        if ($candidato['idade'] > 45) {
            $quantidadeDeHomensComMaisDe45Anos++;
        }

    } else {
        $numeroCandidatosSexoFeminino++;

        if ($candidato['idade'] < 35 && $candidato['experiencia_no_servico']) {
            $numeroDeMulheresComIdadeInferiorA35AnosComExpecienciaNoServico++;
        }

        if ($candidato['experiencia_no_servico'] && $candidato['idade'] < $menorIdadeEntreAsMulheresQueJaTemExperienciaNoServico) {
            $menorIdadeEntreAsMulheresQueJaTemExperienciaNoServico = $candidato['idade'];
        }

    }

}

