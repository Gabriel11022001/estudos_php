<?php

require_once './sortear_array_numerico.php';

$numeros = $sortearNumeros(1000);
$numero = 12;
$quantidadeVezesNumeroApareceNoArray = function (int $numero, array $numeros) {
    $quantidadeVezesNumeroApareceNoArray = 0;
    foreach ($numeros as $num) {
        if ($num === $numero) {
            $quantidadeVezesNumeroApareceNoArray++;
        }
    }
    return $quantidadeVezesNumeroApareceNoArray;
};
$quantidadeVezes = $quantidadeVezesNumeroApareceNoArray($numero, $numeros);
foreach ($numeros as $elemento) {
    if ($elemento === $numero) {
        echo '<p style="background-color: red; color: white; text-align: center; font-size: 20px;">' . $elemento . '</p>';
    } else {
        echo '<p style="background-color: gray; color: white; text-align: center; font-size: 20px;">' . $elemento . '</p>';
    }
}
echo 'Quantidade de vezes que o número ' . $numero . ' aparece no array: ' . $quantidadeVezes . '<br>';