<?php

$pessoas = [
    [
        'nome' => 'Gabriel',
        'idade' => 21,
        'altura' => 1.87
    ],
    [
        'nome' => 'Pedro',
        'idade' => 55,
        'altura' => 1.87
    ],
    [
        'nome' => 'José',
        'idade' => 56,
        'altura' => 1.65
    ],
    [
        'nome' => 'Maria',
        'idade' => 32,
        'altura' => 1.65
    ],
    [
        'nome' => 'Helena',
        'idade' => 87,
        'altura' => 1.54
    ],
    [
        'nome' => 'Luana',
        'idade' => 18,
        'altura' => 1.52
    ]
];
$somaAlturasPessoasComMaisDe50Anos = 0;
$mediaAlturasPessoasComMaisDe50Anos = 0;
$quantidadePessoasComMaisDe50Anos = 0;
$pessoasComMaisDe50Anos = [];

foreach ($pessoas as $pessoa) {

    if ($pessoa['idade'] > 50) {
        $pessoasComMaisDe50Anos[] = $pessoa;
        $quantidadePessoasComMaisDe50Anos++;
        $somaAlturasPessoasComMaisDe50Anos += $pessoa['altura'];
    }

}

$mediaAlturasPessoasComMaisDe50Anos = $somaAlturasPessoasComMaisDe50Anos / $quantidadePessoasComMaisDe50Anos;
echo 'Quantidade de pessoas que possuem mais de 50 anos de idade: ' . $quantidadePessoasComMaisDe50Anos . '<br>';
echo 'Média das alturas das pessoa com mais de 50 anos de idade: ' . number_format($mediaAlturasPessoasComMaisDe50Anos, 2) . '<br>';
echo 'Pessoas que possuem mais de 50 anos de idade:<br>';

for ($i = 0; $i < count($pessoasComMaisDe50Anos); $i++) {
    echo '=============================================<br>';
    echo 'Nome: ' . $pessoasComMaisDe50Anos[$i]['nome'] . '<br>';
    echo 'Idade: ' . $pessoasComMaisDe50Anos[$i]['idade'] . '<br>';
    echo 'Altura: ' . $pessoasComMaisDe50Anos[$i]['altura'] . '<br>';
    
    if ($i === count($pessoasComMaisDe50Anos) - 1) {
        echo '=============================================<br>';
    }

}