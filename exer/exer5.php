<?php

$primeiroNumero = rand(1, 100);
$segundoNumero = rand(1, 100);
$terceiroNumero = rand(1, 100);
$soma = $primeiroNumero + $segundoNumero + $terceiroNumero;
$produto = $primeiroNumero * $segundoNumero * $terceiroNumero;
$mediaInteira = intval(($primeiroNumero + $segundoNumero + $terceiroNumero) / 3);
$maior = 0;
$menor = 0;
echo 'Primeiro número: ' , $primeiroNumero . '<br>';
echo 'Segundo número: ' , $segundoNumero . '<br>';
echo 'Terceiro número: ' . $terceiroNumero . '<br>';

if ($primeiroNumero == $segundoNumero && $segundoNumero == $terceiroNumero) {
    echo 'Todos os números são iguais!<br>';
} else {

    if ($primeiroNumero > $segundoNumero && $primeiroNumero > $terceiroNumero) {
        $maior = $primeiroNumero;
    } elseif ($segundoNumero > $primeiroNumero && $segundoNumero > $terceiroNumero) {
        $maior = $segundoNumero;
    } elseif ($terceiroNumero > $primeiroNumero && $terceiroNumero > $segundoNumero) {
        $maior = $terceiroNumero;
    }

    if ($primeiroNumero < $segundoNumero && $primeiroNumero < $terceiroNumero) {
        $menor = $primeiroNumero;
    } elseif ($segundoNumero < $primeiroNumero && $segundoNumero < $terceiroNumero) {
        $menor = $segundoNumero;
    } elseif ($terceiroNumero < $primeiroNumero && $terceiroNumero < $segundoNumero) {
        $menor = $terceiroNumero;
    }

    echo 'Maior número: ' . $maior . '<br>';
    echo 'Menor número: ' . $menor . '<br>';
}

echo 'Soma dos números: ' . $soma . '<br>';
echo 'Produto: ' . $produto . '<br>';
echo 'Média: ' . $mediaInteira . '<br>';