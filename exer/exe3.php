<?php

function verificarSeExistemNumerosIguaisAoSorteado(int $numeroSorteado, array $numerosMegaSena): bool
{   
    $existe = false;
    foreach ($numerosMegaSena as $numero) {
        if ($numero == $numeroSorteado) {
            $existe = true;
            break;
        }
    }
    return $existe;
}
$numerosMegaSena = [];
for ($contador = 0; $contador < 6; $contador++) {
    if ($contador === 0) {
        $numerosMegaSena[0] = rand(1, 60);
        continue;
    }
    $numeroSorteado = rand(1, 60);
    while (verificarSeExistemNumerosIguaisAoSorteado($numeroSorteado, $numerosMegaSena)) {
        $numeroSortedo = rand(1, 60);
    }
    $numerosMegaSena[$contador] = $numeroSorteado;
}
echo 'Números da mega sena:<br>';
echo '<pre>';
print_r($numerosMegaSena);
echo '</pre>';