<?php

require_once './sortear_array_numerico.php';

$numeros = $sortearNumeros(20);
// ordenar de forma crescente.
for ($i = 0; $i < count($numeros); $i++) {
    $indiceInternoAtual = $i + 1;
    for ($x = $indiceInternoAtual; $x < count($numeros); $x++) {
        if ($numeros[$i] > $numeros[$x]) {
            $aux = $numeros[$i];
            $numeros[$i] = $numeros[$x];
            $numeros[$x] = $aux;
        }
    }
}
echo '<pre>';
print_r($numeros);
echo '</pre>';