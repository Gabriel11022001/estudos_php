<?php

$sortearNumeros = function (int $quantidadeDeElementos): array
{
    $numeros = [];
    $contador = 0;
    while ($contador < $quantidadeDeElementos) {
        $numeros[] = rand(1, 100);
        $contador++;
    }
    return $numeros;
};