<?php

require_once 'src/ProfissionalTI.php';
require_once 'src/ProgramadorPHP.php';

use poo2\ProgramadorPHP;

$programadorPhp = new ProgramadorPHP(
    'Gabriel',
    '123.456.789-00',
    '12.345.678/09',
    ['Laravel', 'CakePHP']
);
$programadorPhp->apresentarDados();