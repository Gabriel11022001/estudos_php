<?php

namespace poo2;

use DateInterval;
use DateTime;
use Exception;

class Carro
{
    private $modelo;
    private $anoLancamento;
    private $velocidadeMaxima;
    private $velocidadeAtual;

    public function __construct($modelo, $anoLancamento, $velocidadeMaxima) {
        $this->velocidadeAtual = 0;
        $this->setModelo($modelo);
        $this->setAnoLancamento($anoLancamento);
        $this->setVelocidadeMaxima($velocidadeMaxima);
    }

    public function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    public function getModelo() {

        return $this->modelo;
    }

    public function setAnoLancamento($anoLancamento) {
        $anoAtual = new DateTime();

        if ($anoAtual->format('Y-m-d') < $anoLancamento->format('Y-m-d')) {
            throw new Exception('Ano de lançamento inválido!');
        }

        $this->anoLancamento = $anoLancamento;
    }

    public function getAnoLancamento() {

        return $this->anoLancamento;
    }

    public function setVelocidadeMaxima($velocidadeMaxima) {

        if ($velocidadeMaxima <= 0) {
            throw new Exception('Não é possível um carro possuir uma velocidade máxima tão pequena!');
        }

        if ($this->velocidadeAtual > 0) {   
            throw new Exception('O carro está em movimento!');
        }

        $this->velocidadeMaxima = $velocidadeMaxima;
    }

    public function getVelocidadeMaxima() {
        
        return $this->velocidadeMaxima;
    }

    public function acelerar() {

        if (($this->velocidadeAtual + 5) > $this->velocidadeMaxima) {
            echo 'Não é possível acelerar mais!<br>';
            return;
        }

        $this->velocidadeAtual += 5;
        echo 'Velocidade atual: ' . $this->velocidadeAtual . ' km/h<br>';
    }

    public function frear() {

        if (($this->velocidadeAtual - 5) <= 0) {
            $this->velocidadeAtual = 0;
        } else {
            $this->velocidadeAtual -= 5;
        }

        echo 'Velocidade atual: ' . $this->velocidadeAtual . 'km/h<br>';
    }
}