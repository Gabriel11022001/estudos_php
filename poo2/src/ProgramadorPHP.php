<?php

namespace poo2;

class ProgramadorPHP extends ProfissionalTI
{
    private $frameworksPhpQueDomina;

    public function __construct($nome, $cpf, $rg, $frameworks) {
        parent::__construct($nome, $cpf, $rg);
        $this->frameworksPhpQueDomina = $frameworks;
    }

    // sobreescrevendo o método apresentarDados() da classe mãe
    public function apresentarDados() {
        parent::apresentarDados();
        echo 'Frameworks php que domina:<br>';

        foreach ($this->frameworksPhpQueDomina as $framework) {
            echo $framework . '<br>';
        }

    }
}