<?php

namespace poo2;

interface IVeiculo
{

    function acelerar();
    function frear();
    function apresentarDadosVeiculo();
}