<?php

namespace poo2;

class Funcionario
{
    /**
     * private -> somente a própria classe pode ter acesso
     * public -> pode ser acessado no escopo da classe
     * e pelo objeto
     * protected -> pode ser acessado no escopo da classe
     * e pelas classes filhas, mas não pode ser acessado
     * por meio do objeto
     */
    private $nome;
    protected $salario;

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getNome() {

        return $this->nome;
    }

    public function setSalario($salario) {
        $this->salario = $salario;
    }

    public function getSalario() {

        return $this->salario;
    }
}