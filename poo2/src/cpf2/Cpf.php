<?php

namespace poo2\cpf2;

use poo2\Documento;

class Cpf extends Documento
{

    public function __construct($numero) {
        parent::__construct($numero);
    }

    public function setNumero($numero) {
        // validar cpf
        if ($this->validar($numero) === false) {
            echo 'Cpf inválido!<br>';
        } else {
            $this->numero = $numero;
        }
    }

    protected function validar($numero) {
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $numero );
            
        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {

            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {

            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {

                return false;
            }
        }

        return true;
    }
}