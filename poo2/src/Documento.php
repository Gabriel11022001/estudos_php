<?php

namespace poo2;

/**
 * Como a classe Documento é uma classe abstrata,
 * eu não posso instanciar objetos dessa classe, eu só posso
 * instanciar objetos de classes concretas
 */
abstract class Documento
{
    protected $numero;

    public function __construct($numero) {
        $this->setNumero($numero);
    }

    public abstract function setNumero($numero);

    public function getNumero() {

        return $this->numero;
    }

    protected abstract function validar($numero);
}