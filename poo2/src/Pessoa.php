<?php

namespace poo2;

use DateTime;

class Pessoa
{
    public $nome;
    public $sexo;
    public $idade;

    public function apresentarDadosPessoa() {
        echo 'Nome: ' . $this->nome . '<br>';
        echo 'Sexo:' . $this->sexo . '<br>';
        echo 'Idade: ' . $this->idade . '<br>';
    }

    public function calcularIdade($dataNascimento) {
        $dataAtual = new DateTime();

        return $dataAtual->diff($dataNascimento)->y;
    }

    public function __toString() {
        
        return 'Nome: ' . $this->nome . ' | Sexo: ' . $this->sexo . '<br>';
    }
}