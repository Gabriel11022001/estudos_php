<?php

namespace poo2;

class Corola implements IVeiculo
{

    public function acelerar() {
        echo 'Acelerando o corola!<br>';       
    }

    public function frear() {
        echo 'Freando o corola!<br>';
    }

    public function apresentarDadosVeiculo() {
        echo 'Apresentando os dados do corolão top!<br>';
    }
}