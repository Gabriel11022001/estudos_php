<?php

namespace poo2;

class ProfissionalTI
{
    protected $nome;
    protected $cpf;
    protected $rg;

    public function __construct($nome, $cpf, $rg) {
        $this->setNome($nome);
        $this->setCpf($cpf);
        $this->setRg($rg);
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getNome() {

        return $this->nome;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function getCpf() {

        return $this->cpf;
    }

    public function setRg($rg) {
        $this->rg = $rg;
    }

    public function getRg() {

        return $this->rg;
    }

    public function apresentarDados() {
        echo 'Nome: ' . $this->nome . '<br>';
        echo 'Cpf: ' . $this->cpf . '<br>';
        echo 'Rg: ' . $this->rg . '<br>';
    }
}