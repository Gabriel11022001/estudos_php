<?php

require_once 'src/Cpf.php';
require_once 'src/ValidaCNPJ.php';

use poo2\Cpf;
use poo2\ValidaCNPJ;

try {
    $cpf = new Cpf();
    // $cpf->setNumeroDocumento('123.456.789-00');
    $cpf->setNumeroDocumento('022.363.190-61');
    echo 'Cpf: ' . $cpf->getNumeroDocumento() . '<br>';
    // validar cnpj invocando método estático
    $cnpj = '96.827.956/0001-30';
    $cnpjValido = ValidaCNPJ::validaCnpj($cnpj);

    if ($cnpjValido) {
        echo 'O cnpj ' . $cnpj . ' é válido!<br>';
    } else {
        echo 'O cnpj ' . $cnpj . ' não é válido!<br>';
    }

} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}