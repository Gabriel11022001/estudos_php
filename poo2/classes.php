<?php

require_once 'src/Pessoa.php';
require_once 'src/Carro.php';

use poo2\Carro;
use poo2\Pessoa;

date_default_timezone_set('America/Sao_Paulo');
$pessoa = new Pessoa();
$pessoa->nome = 'Gabriel Rodrigues dos Santos';
$pessoa->idade = 21;
$pessoa->sexo = 'masculino';
$pessoa->apresentarDadosPessoa();
echo $pessoa->calcularIdade(new DateTime('11-02-2001')) . '<br>';

try {
    $carro = new Carro('Carro1', new DateTime('01-01-2023'), 100);
    
    for ($i = 0; $i < 100; $i++) {
        $carro->acelerar();
    }

    for ($contador = 0; $contador < 100; $contador++) {
        $carro->frear();
    }

} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}