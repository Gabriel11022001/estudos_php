<?php

require_once 'src/IVeiculo.php';
require_once 'src/Corola.php';

use poo2\Corola;

$corola = new Corola();
$corola->acelerar();
$corola->frear();
$corola->apresentarDadosVeiculo();