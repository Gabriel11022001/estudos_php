<?php

try {
    $pdo = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = 'UPDATE tbl_produtos SET nome = :nome, preco = :preco, status = :status,
    descricao_resumida = :descricao_resumida, descricao_completa = :descricao_completa,
    categoria_id = :categoria_id
    WHERE id = :id;';
    $stmt = $pdo->prepare($query);
    $stmt-> bindValue(':id', 1, PDO::PARAM_INT);
    $stmt->bindValue(':nome', 'Produto após edição', PDO::PARAM_STR);
    $stmt->bindValue(':descricao_resumida', 'Descrição resumida do produto após edição', PDO::PARAM_STR);
    $stmt->bindValue(':descricao_completa', 'Descrição completa do produto após edição', PDO::PARAM_STR);
    $stmt->bindValue(':preco', 100, PDO::PARAM_STR);
    $stmt->bindValue(':status', false, PDO::PARAM_BOOL);
    $stmt->bindValue(':categoria_id', 2, PDO::PARAM_INT);

    if ($stmt->execute()) {
        echo 'Os dados do produto foram alterados com sucesso!<br>';
    } else {
        echo 'Ocorreu um erro ao tentar-se editar os dados do produto!<br>';
    }

} catch (Exception $e) {
    echo 'Ocorreu o seguinte erro: ' . $e->getMessage() . '<br>';
}