<?php

try {
    $pdo = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // $pdo->prepare(): método que prepara uma query.
    $stmt = $pdo->prepare('INSERT INTO tbl_produtos(nome, descricao_resumida, descricao_completa,
    preco, status, categoria_id) VALUES(:nome, :descricao_resumida, :descricao_completa,
    :preco, :status, :categoria_id);');
    /**
     * $stmt->bindValue(':coluna', 'valor', PDO::<tipo de dados>);
     */
    $stmt->bindValue(':nome', 'Produto 1', PDO::PARAM_STR);
    $stmt->bindValue(':descricao_resumida', 'Descrição do produto 1', PDO::PARAM_STR);
    $stmt->bindValue(':descricao_completa', 'Descrição completa do produto 1', PDO::PARAM_STR);
    $stmt->bindValue(':preco', 12.50, PDO::PARAM_STR);
    $stmt->bindValue(':status', true, PDO::PARAM_BOOL);
    $stmt->bindValue(':categoria_id', 1, PDO::PARAM_INT);

    // $stmt->execute(): executa a query.
    if ($stmt->execute()) {
        echo 'Produto cadastrado com sucesso!<br>';
    } else {
        echo 'Ocorreu um erro ao tentar-se cadastrar o produto no banco de dados!<br>';
    }

} catch (PDOException $e) {
    echo 'Erro: ' . $e->getMessage() . '<br>';
}