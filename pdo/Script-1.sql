create table tbl_categorias(
	id serial not null primary key,
	descricao text not null
);
create table tbl_produtos(
	id serial not null primary key,
	nome varchar(255) not null unique,
	descricao_resumida text not null,
	descricao_completa text not null,
	preco decimal not null,
	status boolean not null default true,
	categoria_id int not null,
	constraint fk_categoria_id foreign key(categoria_id) references tbl_categorias(id)
);
-- insert into tbl_categorias(descricao) values('Refrigerante');
insert into tbl_produtos(nome, descricao_resumida, descricao_completa, preco, status, categoria_id)
values('Coca-Cola de 2 litros', 'Coca-Cola de 2 litros gelada', 'Coca-Cola de 2 litros gelada muito top',
12.90, true, 1);
select * from tbl_produtos;

