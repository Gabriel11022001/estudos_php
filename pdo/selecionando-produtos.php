<?php

try {
    $pdo = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $pdo->prepare('SELECT p.nome, p.descricao_resumida, p.descricao_completa, p.preco, c.descricao as descricao_categoria FROM tbl_produtos as p
    INNER JOIN tbl_categorias as c ON p.categoria_id = c.categoria_id;');
    $stmt->execute();
    echo '<pre>';

    while ($produto = $stmt->fetch(PDO::FETCH_OBJ)) {
        echo '===========================================<br>';
        echo 'Nome do produto: ' . $produto->nome . '<br>';
        echo 'Descrição resumida: ' . $produto->descricao_resumida . '<br>';
        echo 'Descrição completa: ' . $produto->descricao_completa . '<br>';
        echo 'Preço: R$' . $produto->preco . '<br>';
        echo 'Categoria: ' . $produto->descricao_categoria . '<br>';
        echo '===========================================<br>';
    }

    echo '</pre>';
} catch (PDOException $e) {
    echo 'Erro: ' . $e->getMessage() . '<br>';
}