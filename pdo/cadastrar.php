<?php

$pdo = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try {
    $pdo->beginTransaction(); // iniciando a transação
    // echo 'Conectado com sucesso no banco de dados!<br>';
    // cadastrando categorias
    $categorias = [
        [
            'descricao' => 'Doce'
        ],
        [
            'descricao' => 'Salgado'
        ],
        [
            'descricao' => 'Suco'
        ]
    ];
    // definindo o comando sql para cadastrar a categoria
    $sqlCadastrarCategoria = 'INSERT INTO tbl_categorias(descricao) VALUES(:descricao);';

    foreach ($categorias as $categoria) {
        $stmt = $pdo->prepare($sqlCadastrarCategoria);
        $stmt->bindValue(':descricao', $categoria['descricao']);

        if ($stmt->execute()) {
            echo 'Categoria cadastrada com sucesso!<br>';
        } else {
            throw new PDOException('Ocorreu um erro ao tentar-se cadastrar a categoria no banco de dados!');
        }

    }

    $nome = 'Refrigerante Estrela';
    $descricaoResumida = '';
    $descricaoCompleta = '';
    $preco = 12.98;
    $status = true;
    $categoriaId = 1;
    $sqlCadastrarProduto = 'INSERT INTO tbl_produtos(nome, descricao_resumida, descricao_completa,
    preco, status, categoria_id) VALUES(:nome, :descricao_resumida, :descricao_completa,
    :preco, :status, :categoria_id);';
    $stmt = $pdo->prepare($sqlCadastrarProduto);
    $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);
    $stmt->bindValue(':descricao_resumida', $descricaoResumida);
    $stmt->bindValue(':descricao_completa', $descricaoCompleta);
    $stmt->bindValue(':preco', $preco);
    $stmt->bindValue(':status', $status, PDO::PARAM_BOOL);
    $stmt->bindValue(':categoria_id', $categoriaId, PDO::PARAM_INT);

    if ($stmt->execute()) {
        echo 'Produto cadastrado com sucesso!<br>';
    } else {
        throw new PDOException('Ocorreu um erro ao tentar-se cadastrar o produto!');
    }

    $pdo->commit();
} catch (Exception $e) {
    echo 'Ocorreu o seguinte erro: ' . $e->getMessage() . '<br>';
    $pdo->rollBack(); // cancelando a transação
}