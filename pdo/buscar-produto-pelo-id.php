<?php

try {
    $pdo = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = 'SELECT p.nome, p.preco, p.status, c.descricao as descricao_produto
    FROM tbl_produtos as p INNER JOIN tbl_categorias as c
    ON p.categoria_id = c.categoria_id
    AND p.id = :id
    ORDER BY p.nome;';
    $stmt = $pdo->prepare($query);
    $stmt->bindValue(':id', 1, PDO::PARAM_INT);
    $stmt->execute();
    $produto = $stmt->fetch(PDO::FETCH_OBJ);
    
    if ($produto != false) {
        echo 'Nome do produto: ' . $produto->nome . '<br>';
        echo 'Preço do produto: R$' . number_format($produto->preco, 2) . '<br>';
    }

} catch (PDOException $e) {
    echo 'Erro: ' . $e->getMessage() . '<br>';
}