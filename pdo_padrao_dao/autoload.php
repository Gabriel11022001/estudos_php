<?php

spl_autoload_register(function ($nomeClasse) {
    $nomeClasse = str_replace('Gabriel\\App', 'src/', $nomeClasse);
    $nomeClasse = str_replace('\\', DIRECTORY_SEPARATOR, $nomeClasse);
    $nomeClasse .= '.php';

    if (file_exists($nomeClasse)) {
        require_once $nomeClasse;
    }

});