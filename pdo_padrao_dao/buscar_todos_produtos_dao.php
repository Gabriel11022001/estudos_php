<?php

require_once 'autoload.php';

use Gabriel\App\ConexaoBancoDados;
use Gabriel\App\ProdutoDAO;

try {
    $conexaoBancoDados = new ConexaoBancoDados();
    $produtoDAO = new ProdutoDAO($conexaoBancoDados);
    echo '<pre>';
    print_r($produtoDAO->buscarTodos());
    echo '</pre>';
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}