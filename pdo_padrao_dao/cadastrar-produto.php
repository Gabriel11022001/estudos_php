<?php

require_once 'autoload.php';

use Gabriel\App\ConexaoBancoDados;

try {
    $conexaoBancoDados = new ConexaoBancoDados();
    
    if ($conexaoBancoDados->criarConexao() === false) {
        throw new Exception('Ocorreu um erro ao tentar-se conectar ao banco de dados!');
    }

    $query = 'INSERT INTO tbl_produtos(nome, descricao_resumida, descricao_completa, preco
    , status, categoria_id) VALUES(:nome, :descricao_resumida, :descricao_completa, :preco
    , :status, :categoria_id);';
    $parametros = [
        ':nome' => [
            'valor' => 'Produto 1000',
            'tipo_parametro' => PDO::PARAM_STR
        ],
        ':descricao_resumida' => [
            'valor' => 'Descrição resumida do produto 1000',
            'tipo_parametro' => PDO::PARAM_STR
        ],
        ':descricao_completa' => [
            'valor' => 'Descrição completa do produto 1000',
            'tipo_parametro' => PDO::PARAM_STR
        ],
        ':preco' => [
            'valor' => 1200,
            'tipo_parametro' => PDO::PARAM_STR
        ],
        ':status' => [
            'valor' => true,
            'tipo_parametro' => PDO::PARAM_BOOL
        ],
        ':categoria_id' => [
            'valor' => 1,
            'tipo_parametro' => PDO::PARAM_INT
        ]
    ];

    if ($conexaoBancoDados->query($query, $parametros)) {
        echo 'Produto cadastrado com sucesso!<br>';
    } else {
        echo 'Ocorreu um erro ao tentar-se cadastrar o produto!<br>';
    }

} catch (Exception $e) {
    echo 'Erro: ' . $e->getMessage() . '<br>';
}