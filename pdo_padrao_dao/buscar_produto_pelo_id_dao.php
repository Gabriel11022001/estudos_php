<?php

require_once 'autoload.php';

use Gabriel\App\{
    ConexaoBancoDados,
    ProdutoDAO
};

try {
    $conexaoBancoDados = new ConexaoBancoDados();
    $produtoDAO = new ProdutoDAO($conexaoBancoDados);
    $produto = $produtoDAO->buscarPeloId(17);

    if ($produto != false) {
        echo 'Dados do produto:<br>';
        echo 'Nome do produto: ' . $produto->nome . '<br>';
        echo 'Descrição resumida do produto: ' . $produto->descricao_resumida . '<br>';
        echo 'Descrição completa do produto: ' . $produto->descricao_completa . '<br>';
        echo 'Preço: R$' . number_format($produto->preco, 2) . '<br>';
        $status = $produto->status == 1;
        echo 'Status: ' . $status . '<br>';
    } else {
        echo 'Produto não cadastrado no banco de dados!<br>';
    }

} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}