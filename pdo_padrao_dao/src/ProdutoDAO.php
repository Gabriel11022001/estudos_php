<?php

namespace Gabriel\App;

use PDO;

class ProdutoDAO
{
    private $conexaoBancoDados;

    public function __construct($conexaoBancoDados) {
        $this->conexaoBancoDados = $conexaoBancoDados;
    }

    public function cadastrar($dadosProduto = []) {

        if ($this->conexaoBancoDados->criarConexao() === false) {

            return false;
        }

        $query = 'INSERT INTO tbl_produtos(nome, descricao_resumida, descricao_completa, preco
        , status, categoria_id) VALUES(:nome, :descricao_resumida, :descricao_completa, :preco
        , :status, :categoria_id);';
        $parametros = [
            ':nome' => [
                'valor' => $dadosProduto['nome'],
                'tipo_parametro' => PDO::PARAM_STR
            ],
            ':descricao_resumida' => [
                'valor' => $dadosProduto['descricao_resumida'],
                'tipo_parametro' => PDO::PARAM_STR
            ],
            ':descricao_completa' => [
                'valor' => $dadosProduto['descricao_completa'],
                'tipo_parametro' => PDO::PARAM_STR
            ],
            ':preco' => [
                'valor' => $dadosProduto['preco'],
                'tipo_parametro' => PDO::PARAM_STR
            ],
            ':status' => [
                'valor' => $dadosProduto['status'],
                'tipo_parametro' => PDO::PARAM_BOOL
            ],
            ':categoria_id' => [
                'valor' => $dadosProduto['categoria_id'],
                'tipo_parametro' => PDO::PARAM_INT
            ]
        ];
        
        return $this->conexaoBancoDados->query($query, $parametros);
    }

    public function buscarTodos() {

        if ($this->conexaoBancoDados->criarConexao() === false) {

            return false;
        }

        $query = 'SELECT * FROM tbl_produtos;';
        $stmt = $this->conexaoBancoDados->select($query);

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function buscarPeloId($id) {

        if ($this->conexaoBancoDados->criarConexao() === false) {

            return false;
        }

        $query = 'SELECT * FROM tbl_produtos WHERE id = :id;';
        $parametros = [':id' => ['valor' => $id, 'tipo_parametro' => PDO::PARAM_INT]];
        $stmt = $this->conexaoBancoDados->select($query, $parametros);

        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function editar($dadosProduto = []) {

    }

    public function remover($id) {

    }
}