<?php

namespace Gabriel\App;

use Exception;
use PDO;

class ConexaoBancoDados
{
    private $conexaoBancoDados;

    public function criarConexao() {

        try {
            $this->conexaoBancoDados = new PDO('pgsql:host=c_db;dbname=banco_estudos_php', 'postgres', 'root');
            $this->conexaoBancoDados->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            return true;
        } catch (Exception $e) {

            return false;
        }

    }

    public function query($query, $parametros = []) {

        if (count($parametros) === 0) {
            throw new Exception('Informe os parâmetros para executar a query!');
        }

        if (strlen($query) === 0) {
            throw new Exception('Informe a query de manipulação do banco de dados!');
        }

        // preparando a query
        $stmt = $this->conexaoBancoDados->prepare($query);

        foreach ($parametros as $parametro => $dadosParametro) {
            $stmt->bindValue($parametro, $dadosParametro['valor'], $dadosParametro['tipo_parametro']);
        }

        return $stmt->execute();
    }

    public function select($query, $parametros = []) {
        $consultaComFiltros = true;

        if (count($parametros) === 0) {
            $consultaComFiltros = false;
        }

        if (strlen($query) === 0) {
            throw new Exception('Informe a query de manipulação do banco de dados!');
        }

        // preparando a query
        $stmt = $this->conexaoBancoDados->prepare($query);

        if ($consultaComFiltros) {

            foreach ($parametros as $parametro => $dadosParametro) {
                $stmt->bindValue($parametro, $dadosParametro['valor'], $dadosParametro['tipo_parametro']);
            }

        }

        $stmt->execute();

        return $stmt;
    }

    public function iniciarTransacao() {
        $this->conexaoBancoDados->beginTransaction();
    }

    public function executarTransacao() {
        $this->conexaoBancoDados->commit();
    }

    public function cancelarTransacao() {
        $this->conexaoBancoDados->rollback();
    }
}