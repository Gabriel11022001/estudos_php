<?php

require_once 'autoload.php';

use Gabriel\App\ConexaoBancoDados;
use Gabriel\App\ProdutoDAO;

try {
    $conexaoBancoDados = new ConexaoBancoDados();
    $produtoDAO = new ProdutoDAO($conexaoBancoDados);
    $dadosProduto = [
        'nome' => 'Produto 2000',
        'descricao_resumida' => 'Descrição resumida do produto 2000',
        'descricao_completa' => 'Descrição completa do produto 2000',
        'preco' => 1300,
        'status' => true,
        'categoria_id' => 1
    ];

    if ($produtoDAO->cadastrar($dadosProduto)) {
        echo 'Produto cadastrado com sucesso!<br>';
    } else {
        echo 'Ocorreu um erro ao tentar-se cadastrar o produto!<br>';
    }

} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
}