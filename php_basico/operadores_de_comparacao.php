<?php

$numero1 = 1;
$numero2 = 2;
// maior que
var_dump($numero1 > $numero2);
echo '<br>';
// menor que
var_dump($numero1 < $numero2);
echo '<br>';
// maior ou igual a
var_dump($numero1 >= $numero2);
echo '<br>';
// menor ou igual a
var_dump($numero1 <= $numero2);
echo '<br>';
$numero1 = 2;
// igual a
var_dump($numero1 == $numero2);
echo '<br>';
$valor1 = '12';
$valor2 = 12;
var_dump($valor1 == $valor2);
echo '<br>';
// estritamente igual
var_dump($valor1 === $valor2);
echo '<br>';
// diferente de
var_dump($valor1 != $valor2);
echo '<br>';
// estritamente diferente de
var_dump($valor1 !== $valor2);
echo '<br>';