<?php

// boolean.
$ehVerdadeiro = 2;
if ($ehVerdadeiro) {
    echo 'É verdadeiro!<br>';
} else {
    echo 'Não é verdadeiro!<br>';
}
// string.
$frase = 'A vida é bela, amar é o segredo da vida.';
echo $frase . '<br>';
echo gettype($frase) . '<br>';
/**
 * - Eu vou utilizar aspas duplas quando eu quiser implementar
 * interpolação.
 */
$nome = 'Gabriel';
$frase = 'A vida é bela meu caro {$nome}<br>';
echo $frase . '<br>';
$frase = "A vida é bela meu caro {$nome}<br>";
echo $frase . '<br>';
// strlen -> retorna a quantidade de caracteres de uma string.
echo strlen($frase) . '<br>';
// mb_strtoupper -> retorna a string com todos os caracteres em maíusculo.
$frase = mb_strtoupper($frase);
echo $frase . '<br>';
// mb_strtolower -> retorna a string com todos os caracteres em minúsculo.
$frase = mb_strtolower($frase);
echo $frase . '<br>';
// str_contains -> retorna se uma string possui uma outra string.
echo str_contains($frase, 'gabriel') . '<br>';
// numbers.
$numero1 = '22.98';
echo gettype($numero1) . '<br>';
$numero2 = 22;
/**
 * - Em php, mesmo que a variável seja uma string, se ela conter
 * um valor numérico e eu tentar somar essa variável com um outro valor
 * numérico, o php vai realizar a soma de forma correta, ou seja, vai considerar
 * o valor atribuído á variável que possui a string, como um valor numérico.
 */
echo $numero1 + $numero2 . '<br>';