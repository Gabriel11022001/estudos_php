<?php

/**
 * - Para destruir uma sessão, é necessário 
 * invocar a função session_destroy();
 */
session_start();
if (isset($_SESSION['nome_usuario']) && isset($_SESSION['email_usuario'])) {
    session_destroy();
} else {
    echo 'A sessão não está definida!<br>';
}
