<?php

$nomes = ['Gabriel', 'Pedro', 'Gustavo', 'Maria'];
$contador = 0;
while ($contador < count($nomes)) {
    echo $nomes[$contador] . '<br>';
    $contador++;
}
$numeros = [1, 55, 22, 33, 42, 11];
$soma = 0;
$contador = 0;
$quantidadeElementosArrayNumeros = count($numeros);
while ($contador < $quantidadeElementosArrayNumeros) {
    $soma += $numeros[$contador];
    $contador++;
}
echo 'Soma: ' . $soma . '<br>';