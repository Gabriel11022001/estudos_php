<?php

$numero = 1;
do {
    echo $numero . '<br>';
    $numero++;
} while($numero <= 100);
$contador = 0;
$soma = 0;
$dados = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
do {
    $soma += $dados[$contador];
    $contador++;
} while($contador < count($dados));
echo 'Soma: ' . $soma . '<br>';