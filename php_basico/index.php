<?php

/**
 * - O código php deve ficar entre as tags <?php ?>
 * - O ideal é somente fechar a tag php se eu for
 * misturar código php com código html.
 */
echo 'Olá, meu nome é Gabriel Rodrigues dos Santos<br>';
$nome = 'Gabriel Rodrigues dos Santos';
echo 'Olá, meu nome é ' . $nome . '<br>';
$primeiroValor = 12;
$segundoValor = 434;
$soma = $primeiroValor + $segundoValor;
echo 'A soma entre ' . $primeiroValor . ' e ' . $segundoValor . ' é igual a ' . $soma . '<br>';