<?php

/**
 * - A constante é uma estrutura de dados que armazena
 * um valor constante, ou seja, um valor que não vai ser alterado
 * durante o decorrer do algoritmo.
 */
const NOME = 'Gabriel';
echo NOME . '<br>';
// NOME = 'Maria';
define('POSSUI_CNH', true);
echo POSSUI_CNH . '<br>';