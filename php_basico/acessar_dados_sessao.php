<?php

session_start();
if (isset($_SESSION['nome_usuario']) && isset($_SESSION['email_usuario'])) {
    echo 'Acessando os dados da sessão!<br>';
    echo '<pre>';
    print_r($_SESSION);
    echo '</pre>';
    echo 'Nome do usuário logado: ' . $_SESSION['nome_usuario'] . '<br>';
    echo 'E-mail do usuário logado: ' . $_SESSION['email_usuario'] . '<br>';
} else {
    echo 'Os dados do usuário logado não estão definidos!<br>';
}