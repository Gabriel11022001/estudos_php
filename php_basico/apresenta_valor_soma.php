<?php

/**
 * - O require apresenta um erro e para a aplicação.
 * - O include apresenta um erro e não para a aplicação.
 * - O require_once apresenta um erro e para a aplicação se o
 * arquivo não existir, além disso, só carrega um arquivo uma única vez.
 * - O include_onde apresenta um erro mas não para a aplicação se o
 * arquivo não existir, além disso, só carrega um arquivo uma única vez.
 */
require_once './soma.php';

echo 'O valor da soma entre ' . $valor1 . ' e ' . $valor2 . ' é igual a ' . $somaValores . '<br>';
?>
<h1>Olá Mundo!</h1>