<?php

function somar(float $primeiroValor, float $segundoValor): float
{
    return $primeiroValor + $segundoValor;
}
function subtrair(float $primeiroValor, float $segundoValor): float
{
    return $primeiroValor - $segundoValor;
}
$multiplicacao = function (float $primeiroValor, $segundoValor): float
{
    return $primeiroValor * $segundoValor;
};
$soma = somar(12, 4);
$multiplicacao = $multiplicacao(12, 2);
echo 'O valor da soma é: ' . $soma . '<br>';
echo 'O valor da multiplicação é: ' . $multiplicacao . '<br>';
echo 'A subtração é igual a: ' . subtrair(20, 10) . '<br>';
$arr = [12, 66.453, 11, 6, 12, 11, 13, 123];
$somaDosElementosDoArray = function (array $arr) {
    $somaDosElementosDoArray = 0;
    foreach ($arr as $elemento) {
        $somaDosElementosDoArray += $elemento;
    }
    return $somaDosElementosDoArray;
};
echo '<pre>';
print_r($arr);
echo '</pre>';
echo 'Soma dos elementos do array: ' . $somaDosElementosDoArray($arr) . '<br>';