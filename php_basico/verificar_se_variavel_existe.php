<?php

/**
 * - Varificamos se um avariável existe por meio do retorno
 * da função isset();
 */
// $nome = 'Gabriel Rodrigues dos Santos'; -> true
// $nome = ''; -> true
// $nome; -> false
// $nome = null; -> false
$nome = 'Gabriel';
var_dump(isset($nome));
echo '<br>';
if (isset($nome)) {
    echo 'A variável $nome foi definida e está guardando o seguinte nome: ' . $nome . '<br>';
} else {
    echo 'A variável $nome não existe!<br>';
}