<?php

$primeiroValor = 12;
$segundoValor = 44;
// soma.
echo 'Soma: ' . ($primeiroValor + $segundoValor) . '<br>';
// subtração.
echo 'Subtração: ' . ($primeiroValor - $segundoValor) . '<br>';
// multiplicação.
echo 'Multiplicação: ' . ($primeiroValor * $segundoValor) . '<br>';
// divisão.
echo 'Divisão: ' . ($primeiroValor / $segundoValor) . '<br>';
// resto da divisão ou módulo.
echo 'Módulo: ' . ($segundoValor % $primeiroValor) . '<br>';
/**
 * precedência:
 * 1° -> ()
 * 2° -> **
 * 3° -> * / %
 * 4° -> + - .
 */
echo 7 * 2 + 3 . '<br>'; // 17
echo 10 + 2 / 2 . '<br>'; // 11
echo 7 * (2 + 3) . '<br>'; // 35
echo 10 / 2 * 3 . '<br>'; // 15 -> primeiro vai calcular a divisão.
echo 2 * 3 / 2 . '<br>'; // 3 -> primeiro vai fazer a multiplicação.