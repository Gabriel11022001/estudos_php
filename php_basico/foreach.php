<?php

$nomes = ['Gabriel', 'Pedro', 'Gustavo', 'Maria'];
foreach ($nomes as $nome) {
    echo $nome . '<br>';
}
$pessoas = [
    'gabriel' => [
        'idade' => 21,
        'sexo' => 'm'
    ],
    'maria' => [
        'idade' => 22,
        'sexo' => 'f'
    ]
];
foreach ($pessoas as $nomePessoa => $dadosPessoa) {
    echo $nomePessoa . '<br>';
    echo $dadosPessoa['idade'] . '<br>';
    echo $dadosPessoa['sexo'] . '<br>';
}