<?php

$dados = ['Gabriel', 'Maria', 'João', 'Pedro'];
echo '<pre>';
print_r($dados);
echo '</pre>';
$dados[] = true;
$dados[] = 'José';
$dados[] = 22.978;
$dados[] = 12;
$dados[] = [
    'Gustavo',
    'Maria',
    'Antônio'
];
echo '<pre>';
var_dump($dados);
echo '</pre>';
var_dump($dados[0]);
echo '<br>';
var_dump($dados[1]);
echo '<br>';
echo '<pre>';
// count -> retorna o número de elementos de um array.
var_dump($dados[count($dados) - 1]);
echo '</pre>';
/**
 * - Existem duas formar de adicionar elementos no final do array,
 * posso utiliza:
 * -> $arr[] = ''
 * ou
 * -> array_push($arr, '')
 */
$dados[] = 'Qualquer valor!';
array_push($dados, 'Adicionando mais um valor no final!');
echo '<pre>';
print_r($dados);
echo '</pre>';
array_unshift($dados, 'Adicionando essa string no início do array!');
echo '<pre>';
print_r($dados);
echo '</pre>';
// arrays associativos.
$clientes = [
    'Gabriel' => [
        'idade' => 21,
        'sexo' => 'M'
    ],
    'Maria' => [
        'idade' => 44,
        'sexo' => 'F'
    ]
];
echo '<pre>';
print_r($clientes);
echo '</pre>';
echo $clientes['Gabriel']['idade'] . '<br>';
echo $clientes['Gabriel']['sexo'] . '<br>';
echo $clientes['Maria']['idade'] . '<br>';
echo $clientes['Maria']['sexo'] . '<br>';