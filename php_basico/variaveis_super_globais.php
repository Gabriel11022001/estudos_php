<?php

session_start();
/**
 * - $_COOKIE
 * - Para criar um cookie utiliza-se a função setcookie('chave', 'valor', data de expiração);
*/
setcookie('nome', 'Gabriel Rodrigues dos Santos', time() + 2 * 24 * 60 * 60);
echo $_COOKIE['nome'] . '<br>';
/**
 * - $_SESSION
 * - Para poder utilizar as sessões, eu devo invocar a função
 * session_start();
 */
$_SESSION['nome_usuario'] = 'Gabriel';
$_SESSION['email_usuario'] = 'gabriel@gmail.com';
echo $_SESSION['nome_usuario'] . '<br>';
/**
 * - $_SERVER
 */
echo '<pre>';
print_r($_SERVER);
echo '</pre>';
echo $_SERVER['HTTP_HOST'] . '<br>';
echo $_SERVER['REQUEST_URI'] . '<br>';
echo $_SERVER['REQUEST_METHOD'] . '<br>';
/**
 * - $_GET
 * - $_POST
 */
if (isset($_GET['nome'])) {
    echo $_GET['nome'] . '<br>';
}
if (isset($_GET['email'])) {
    echo $_GET['email'] . '<br>';
}
if (isset($_POST['nome']) && isset($_POST['email'])) {
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    echo 'Nome informado: ' . $nome . '<br>';
    echo 'E-mail informado: ' . $email . '<br>';
} else {
    echo 'Os dados da pessoa ainda não foram informados!<br>';
}
echo '<pre>';
print_r($_FILES);
echo '<pre>';
if (isset($_FILES['arquivo']['name'])) {
    echo 'Nome do arquivo enviado: ' . $_FILES['arquivo']['name'] . '<br>';
} else {
    echo 'O arquivo não foi enviado!<br>';
}