<?php

// atribuir valor á variável.
$nome = 'Gabriel';
echo $nome . '<br>';
$ultimoNome = 'Santos';
// concatenação.
$nome = $nome . ' ' . $ultimoNome;
echo $nome . '<br>';
$nome .= ' | Um cara muito legal';
echo $nome . '<br>';
$mensagem = "Olá $nome, é um prazer te conhecer!<br>"; // interpolação.
echo $mensagem . '<br>';
// atribuição com soma.
$valor1 = 10;
$valor1 += 2; // 12
echo $valor1 . '<br>';
// atribuição com subtração;
$valor1 = 100;
$valor1 -= 10; // 90
echo $valor1 . '<br>';
// atribuição com multiplicação.
$valor1 *= 2; // 180
echo $valor1 . '<br>';
// atribuição com divisão.
$valor1 /= 2; // 90
echo $valor1 . '<br>';