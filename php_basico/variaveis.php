<?php

// strings.
$nomeCompleto = 'Gabriel Rodrigues dos Santos';
echo gettype($nomeCompleto) . '<br>';
$mensagem = 'Fala meu querido, bom dia!';
echo $mensagem . '<br>';
echo gettype($mensagem) . '<br>';
// integers.
$idade = 21;
echo $idade . '<br>';
echo gettype($idade) . '<br>';
// double.
$peso = 87.8;
echo $peso . '<br>';
echo gettype($peso) . '<br>';
// boolean.
$possuiCnh = true;
echo $possuiCnh . '<br>';
echo gettype($possuiCnh) . '<br>';
$possuiCnh = false;
echo $possuiCnh . '<br>';
echo gettype($possuiCnh) . '<br>';
// object.
class Pessoa
{
    public string $nome;
    public int $idade;
}
$pessoa = new Pessoa();
echo gettype($pessoa) . '<br>';
$pessoa->nome = 'Gabriel';
$pessoa->idade = 21;
echo $pessoa->nome . '<br>';
echo $pessoa->idade . '<br>';
// array.
$pessoas = [
    'Gabriel',
    'Maria',
    'Pedro',
    'Maria Eduarda'
];
echo '<pre>';
print_r($pessoas);
echo '</pre>';
echo gettype($pessoas) . '<br>';
/**
 * - PHP é uma linguagem case-sensitive, ou seja, $nome 
 * é diferente de $Nome.
 */
$sexo = 'Masculino';
$SExo = 'Feminino';
echo $sexo . '<br>'; // $sexo é uma variável.
echo $SExo . '<br>'; // $SExo é outra variável.