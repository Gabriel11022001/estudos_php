<?php

$nome = 'José';
switch ($nome) {
    case 'Pedro':
        echo 'O nome é Pedro!<br>';
        break;
    case 'Maria':
        echo 'O nome é Maria!<br>';
        break;
    case 'Gabriel':
        echo 'O nome é Gabriel!<br>';
        break;
    default:
        echo 'Nenhum nome!<br>';
}