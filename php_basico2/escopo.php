<?php

$nome = 'Gabriel';

if ($nome === 'Gabriel') {
    echo $nome . '<br>';
    $nome = 'Gabriel Rodrigues dos Santos';
}

echo $nome . '<br>';

function funcaoQualquer() {
    // echo $nome . '<br>';
    global $nome;
    $nome = 'Pedro';
}

function alterarValorVariavel(&$variavel) {
    $variavel = 'Valor após alteração!';
}

funcaoQualquer();
echo $nome . '<br>';
$sexo = 'Masculino';
echo $sexo . '<br>';
alterarValorVariavel($sexo);
echo $sexo . '<br>';