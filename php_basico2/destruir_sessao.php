<?php

session_start();

if (isset($_SESSION)) {
    // função que destroi todos os dados da sessão
    session_destroy();
} else {
    echo 'Sessão não definida!';
}