<?php

date_default_timezone_set('America/Sao_Paulo');
// função date()
echo date('d-m-Y') . '<br>';
echo date('Y-m-d') . '<br>';
echo date('d-m-Y H:i:s') . '<br>';
echo date('d/m/Y') . '<br>';
$dataHoje = new DateTime();
echo $dataHoje->format('d/m/Y') . '<br>';
echo $dataHoje->format('Y-m-d') . '<br>';
// somando datas
// P - Período 15 - Quantidade de D - Dias
$dataDaqui15Dias = $dataHoje->add(new DateInterval('P15D'));
echo $dataDaqui15Dias->format('d-m-Y') . '<br>';
// P - Período 1 - Quantidade de M - Meses
$dataDaqui1Mes = $dataDaqui15Dias->add(new DateInterval('P1M'));
echo $dataDaqui1Mes->format('d/m/Y') . '<br>';
$diferencaDias = $dataDaqui15Dias->diff(new DateTime())->d;
echo $diferencaDias . '<br>';