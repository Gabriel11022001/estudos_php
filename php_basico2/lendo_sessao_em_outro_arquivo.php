<?php

session_start();

if (isset($_SESSION['nome'])
&& isset($_SESSION['email'])
&& isset($_SESSION['telefone'])) {
    echo 'Nome:' . $_SESSION['nome'] . '<br>';
    echo 'Email: ' . $_SESSION['email'] . '<br>';
    echo 'Telefone: ' . $_SESSION['telefone'] . '<br>';
} else {
    echo 'A sessão não foi definida!';
}