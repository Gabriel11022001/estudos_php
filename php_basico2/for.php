<?php

for ($i = 1; $i <= 10; $i++) {
    echo $i . '<br>';
}

$nomes = ['Gabriel', 'Maria', 'Pedro', 'Manuel'];

for ($i = 0; $i < count($nomes); $i++) {
    echo $nomes[$i] . '<br>';
}

$valores = [1, 2, 33, 2, 4, 5, 6];
$soma = 0;

for ($i = 0; $i < count($valores); $i++) {
    $soma += $valores[$i];
}

echo 'Soma: ' . $soma . '<br>';