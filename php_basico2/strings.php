<?php

$nome = 'Gabriel';
$sobrenome = 'Rodrigues';
// no exemplo abaixo não vai interpolar.
$nomeCompletoComAspasSimples = '$nome $sobrenome';
/**
 * No exemplo abaixo, a interpolação vai funcionar
 * por conta das aspas duplas.
 */
$nomeCompletoComAspasDuplas = "$nome $sobrenome";
echo $nomeCompletoComAspasSimples . '<br>';
echo $nomeCompletoComAspasDuplas . '<br>';
$nomeEmMaiusculo = strtoupper($nome);
echo 'Nome em maiusculo: ' . $nomeEmMaiusculo . '<br>';
$nomeEmMinusculo = strtolower($nome);
echo 'Nome em minusculo: ' . $nomeEmMinusculo . '<br>';
$nomeCompletoComAspasDuplas = str_replace('Rodrigues', 'Rodrigues dos Santos', $nomeCompletoComAspasDuplas);
echo $nomeCompletoComAspasDuplas . '<br>';
echo 'Tamanho do nome: ' . strlen($nome) . '<br>';