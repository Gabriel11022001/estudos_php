<?php

$clientes = [
    [
        'id' => 1,
        'nome' => 'Gabriel',
        'sexo' => 'Masculino',
        'idade' => 22
    ],
    [
        'id' => 2,
        'nome' => 'Fernando',
        'sexo' => 'Masculino',
        'idade' => 22
    ],
    [
        'id' => 3,
        'nome' => 'Pedro',
        'sexo' => 'Masculino',
        'idade' => 22
    ]
];
/**
 * A função json_encode($array) converte
 * um array para json
 */
$clientesJson = json_encode($clientes);
echo $clientesJson . '<br>';
/**
 * A função json_decode($json) converte
 * de json para um array de objetos
 */
$clientesConvertidoArray = json_decode($clientesJson);
echo '<pre>';
print_r($clientesConvertidoArray);
echo '</pre>';

foreach ($clientesConvertidoArray as $cliente) {
    echo '=================================<br>';
    echo $cliente->id . '<br>';
    echo $cliente->nome . '<br>';
    echo $cliente->sexo . '<br>';
    echo $cliente->idade . '<br>';
}