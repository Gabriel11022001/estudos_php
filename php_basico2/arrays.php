<?php

/**
 * Posso criar arrays de duas formas:
 * $nomes = [];
 * $nomes = array();
 */
$nomes = ['Gabriel', 'Maria', 'Pedro', 'Gustavo'];
$nomes2 = array('Gabriel', 'Maria', 'Pedro');
echo '<pre>';
print_r($nomes);
print_r($nomes2);
echo '</pre>';
// array bidimensional
$pessoas = [
    [
        'Gabriel',
        'Maria',
        'Pedro'
    ],
    [
        'Eduarda',
        'Maria',
        'Gustavo'
    ]
];
echo '<pre>';
print_r($pessoas);
echo '</pre>';
echo $pessoas[0][0] . '<br>';
echo $pessoas[0][1] . '<br>';
echo $pessoas[0][2] . '<br>';
echo $pessoas[1][0] . '<br>';
echo $pessoas[1][1] . '<br>';
echo $pessoas[1][2] . '<br>';
// adicionando elementos
$nomes[] = 'Fernando'; // vai adicionar na ultima posição
echo '=============================================<br>';
echo $nomes[count($nomes) - 1] . '<br>';
/**
 * A função array_push() adiciona um elemento no ultimo indice
 */
array_push($nomes2, 'Mariano');
echo $nomes2[count($nomes2) - 1] . '<br>';