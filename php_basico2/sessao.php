<?php

/**
 * Para iniciar uma sessão, deve-se invocar a
 * função session_start()
 */
session_start();
$_SESSION['nome'] = 'Gabriel Rodrigues dos Santos';
$_SESSION['email'] = 'gabriel@gmail.com';
$_SESSION['telefone'] = '(14) 99766-8765';
echo 'Nome: ' . $_SESSION['nome'] . '<br>';
echo 'Email: ' . $_SESSION['email'] . '<br>';
echo 'Telefone: ' . $_SESSION['telefone'] . '<br>';