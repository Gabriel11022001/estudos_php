<?php

function validarSeDadosSaoValidos($primeiroValor, $segundoValor) {
    
    if (empty($primeiroValor) || empty($segundoValor)) {
        return false;
    }

    if (is_numeric($primeiroValor) === false
    || is_numeric($segundoValor) === false) {
        
        return false;
    }

    return true;
}

function somar($primeiroValor, $segundoValor) {

    if (validarSeDadosSaoValidos($primeiroValor, $segundoValor) === false) {
        echo 'Dados inválidos!<br>';
        return;
    }

    return $primeiroValor + $segundoValor;
}

$primeiroValor = 12.98;
$segundoValor = 12;
echo somar($primeiroValor, $segundoValor) . '<br>';

function darAumentoParaFuncionario($salarioAtual, $percentualDeAumento) {

    if (empty($salarioAtual)
    || empty($percentualDeAumento)) {
        echo 'Dados inválidos!<br>';

        return;
    }

    if (!is_numeric($salarioAtual)
    || !is_numeric($percentualDeAumento)) {
        echo 'O salário atual e o percentual de aumento devem ser valores'
        . ' numéricos!<br>';

        return;
    }

    if ($salarioAtual <= 0) {
        echo 'O salário não deve ser menor ou igual a R$0!<br>';

        return;
    }

    if ($percentualDeAumento <= 0) {
        echo 'O percentual de aumento não deve ser menor ou igual a 0%!<br>';

        return;
    }

    $novoSalario = $salarioAtual + (($percentualDeAumento / 100) * $salarioAtual);

    return $novoSalario;
}

$salarioAtual = 3000;
$percentualDeAumento = 10;
$novoSalario = darAumentoParaFuncionario($salarioAtual, $percentualDeAumento);

if (isset($novoSalario)) {
    echo 'Novo salário: R$' . $novoSalario . '<br>';
}

$novoSalario = darAumentoParaFuncionario($novoSalario, 20);

if (isset($novoSalario)) {
    echo 'Novo salário: R$' . $novoSalario . '<br>';
}

function incrementarValorVariavel(&$var) {
    $var++;
}

$var = 10;
incrementarValorVariavel($var);
incrementarValorVariavel($var);
incrementarValorVariavel($var);
incrementarValorVariavel($var);
echo $var . '<br>';

function somarVariosValores(...$valores) {

    return array_sum($valores);
}

$soma = somarVariosValores(1, 22, 3.76, 2, 98, 1200);
echo 'Soma: ' . $soma . '<br>';