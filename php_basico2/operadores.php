<?php

$primeiroValor = 12; // atribuição
echo $primeiroValor . '<br>';
$primeiroValor++; // incremento
echo $primeiroValor . '<br>';
$primeiroValor--; // decremento
echo $primeiroValor . '<br>';
$primeiroValor += 10; // incrementando 10
echo $primeiroValor . '<br>';
$primeiroValor -= 5; // decrementando 5
echo $primeiroValor . '<br>';
$primeiroValor *= 2; // atribuindo a variável o valor dela multiplicado por 2
echo $primeiroValor . '<br>';
$soma = 10 + 12; // soma
echo 'Soma: ' . $soma . '<br>';
$subtracao = 12 - 10; // subtração
echo $subtracao . '<br>';
$multiplicacao = 12 * 10; // multiplicação
echo $multiplicacao . '<br>';
$divisao = 10 / 2; // divisão
echo $divisao . '<br>';
$primeiroValor = 10;
$segundoValor = '10';
echo ($primeiroValor == $segundoValor) . '<br>'; // igual a
echo ($primeiroValor === $segundoValor) . '<br>'; // extritamente igual a
echo ($primeiroValor > $segundoValor) . '<br>'; // maior que
echo ($primeiroValor < $segundoValor) . '<br>'; // menor que
echo ($primeiroValor != $segundoValor) . '<br>'; // diferente de
echo ($primeiroValor >= $segundoValor) . '<br>'; // maior ou igual a
echo ($primeiroValor <= $segundoValor) . '<br>'; // menor ou igual a