<?php

/**
 * Constantes são estruturas de dados que assim como as variáveis
 * armazenam dados, porém, esses dados não podem ser alterados
 * durante a execução do programa.
 * Em php posso definir constantes de duas formas:
 * const NOME = 'Gabriel';
 * define('NOME', 'Gabriel Rodrigues dos Santos');
 * O ideal é sempre definir os nomes das contantes tudo em 
 * maíusculo.
 */
const NOME = 'Gabriel Rodrigues';
define('SEXO', 'Masculino');
echo NOME . '<br>';
echo SEXO . '<br>';
// NOME = 'Pedro'; não é possível alterar o valor atribuido a uma constante
define('DB', [
    'servidor' => '127.0.0.1',
    'usuario' => 'root',
    'senha_usuario' => 'root',
    'nome_banco' => 'banco_de_dados_qualquer'
]);
echo 'Servidor: ' . DB['servidor'] . '<br>';
echo 'Usuário: ' . DB['usuario'] . '<br>';
echo 'Senha: ' . DB['senha_usuario'] . '<br>';
echo 'Banco de dados: ' . DB['nome_banco'] . '<br>';
echo 'Versão do php: ' . PHP_VERSION . '<br>';
echo 'Caminho do arquivo: ' . __DIR__ . '<br>';
echo 'Separador de diretórios do sistema operacional: ' . DIRECTORY_SEPARATOR . '<br>';