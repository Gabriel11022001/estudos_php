<?php

// vai carregar o arquivo e se ocorrer um erro não vai parar a aplicação.
// include './somar.php2';
// vai carregar o arquivo e se ocorrer um erro vai parar a aplicação. 
// require './somar2.php';
/**
 * Mesma coisa do include, a diferença é que só carrega o arquivo
 * uma única vez.
 */
// include_once './somar.php';
/**
 * Mesma coisa do require, a diferença é que só carrega o arquivo
 * uma única vez.
 */
require_once './somar.php';

$soma = somar(10, 20);
echo 'Soma: ' . $soma . '<br>';
echo 'Gabriel<br>';