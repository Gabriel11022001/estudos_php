<?php

$nomes = ['Gabriel', 'Maria', 'Pedro', 'Paulo'];

foreach ($nomes as $nome) {
    echo 'Nome: ' . $nome . '<br>';
}

$funcionarios = [
    [
        'nome' => 'Gabriel',
        'salario' => 3400
    ],
    [
        'nome' => 'Matheus',
        'salario'=> 4000
    ],
    [
        'nome' => 'Eduarda',
        'salario' => 9000
    ]
];

$somaSalarios = function ($funcionarios) {
    $somaSalarios = 0;

    foreach ($funcionarios as $funcionario) {
        $somaSalarios += $funcionario['salario'];
    }

    return $somaSalarios;
};
echo 'Soma dos salários: R$' . $somaSalarios($funcionarios) . '<br>';
$apresentarFuncionarios = function ($funcionarios) {

    foreach ($funcionarios as $indice => $funcionario) {
        echo 'Funcionário no índice ' . $indice . '<br>';
        echo 'Nome: ' . $funcionario['nome'] . '<br>';
        echo 'Salário: R$' . $funcionario['salario'] . '<br>';
    }

};
$apresentarFuncionarios($funcionarios);