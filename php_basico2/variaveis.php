<?php

/**
 * O php é uma linguagem de tipagem dinâmica, ou seja,
 * os tipos das variáveis são definidos pelo tipo do dado
 * atribuído a variável.
 * Em php, eu posso atribuir um dado de um tipo a uma variável
 * e em seguida atribuir a mesma, outro dado de outro tipo.
 */
$nome = 'Gabriel Rodrigues dos Santos'; // string
$idade = 21; // inteiro
$sexo = 'Masculino'; // string
$peso = 87.76; // float
$possuiCnh = true; // boolean
echo 'Nome: ' . $nome . '<br>';
echo 'Idade: ' . $idade . ' anos de idade<br>';
echo 'Sexo: ' . $sexo . '<br>';
echo 'Peso: ' . $peso . 'kg<br>';
echo 'Possui cnh?=' . ($possuiCnh ? 'Sim' : 'Não');
echo '<br>';
echo gettype($nome) . '<br>';
echo 'O tipo da variável $nome é: ' . gettype($nome) . '<br>';
echo 'O tipo da variável $idade é: ' . gettype($idade) . '<br>';
echo 'O tipo da variável $peso é: ' . gettype($peso) . '<br>';
$nome = 12345;
echo 'Agora, o tipo da variável $nome é: ' . gettype($nome) . '<br>';
/**
 * Em php, o ideal é utilizar o padrão $camelCase, exemplo:
 */
$nomeCompleto = 'Gabriel Rodrigues dos Santos';
$dataNascimento = '11/02/2001';
echo 'Meu nome é ' . $nomeCompleto . ' e eu nasci em ' . $dataNascimento . '<br>';
/**
 * Na variável $textoComAspasDuplas eu defini a string com aspas duplas,
 * agora eu posso realizar a interpolação dessa variável com outra variável,
 * com string entre aspas simples isso não é possível.
 */
$textoComAspasDuplas = "Texto qualquer acompanhado da variável com o valor $nome<br>";
echo $textoComAspasDuplas;
$frutas = ['Laranja', 'Uva', 'Morango']; // array
echo $frutas[0] . '<br>';
echo $frutas[1] . '<br>';
echo $frutas[2] . '<br>';
echo '<pre>';
print_r($frutas);
echo '</pre>';
echo gettype($frutas) . '<br>';
$dataNascimento = new DateTime('11-02-2001');
echo $dataNascimento->format('d-m-Y') . '<br>';
echo $dataNascimento->format('Y-m-d') . '<br>';