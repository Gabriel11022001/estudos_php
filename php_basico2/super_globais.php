<?php

/**
 * As super globais são arrays pré-definidos pelo próprio php
 * e podem ser acessados em qualquer ponto do meu arquivo
 * com a extensão .php
 */
// $_GET -> dados obtidos por meio do protocolo http get.
$nome = $_GET['nome'];

if (isset($nome)) {
    echo 'Nome: ' . $nome . '<br>';
} else {
    echo 'O nome não foi informado!<br>';
}

echo gettype($nome) . '<br>';
$nome = (int) $nome;
echo gettype($nome) . '<br>';
$sexo = $_GET['sexo'];

if (isset($sexo)) {
    echo 'Sexo: ' . $sexo . '<br>';
} else {
    echo 'Não informou o sexo!<br>';
}

// echo '<pre>';
// print_r($_SERVER);
// echo '</pre>';
// $_SERVER -> super global que possui os dados do servidor.
$ip = $_SERVER['REMOTE_ADDR'];
echo 'Endereço ip: ' . $ip . '<br>';