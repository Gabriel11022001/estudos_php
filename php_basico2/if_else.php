<?php

$idadeCrianca = 12;
$idadeAdolescente = 20;
$idadeAdulto = 40;
$idadeMeiaIdade = 50;
$idadeIdoso = 100;
$minhaIdade = 21;

if ($minhaIdade <= $idadeCrianca) {
    echo 'Sou uma criança!<br>';
} elseif ($minhaIdade <= $idadeAdolescente) {
    echo 'Sou um adolescente!<br>';
} elseif ($minhaIdade <= $idadeAdulto) {
    echo 'Sou um adulto!<br>';
} elseif ($minhaIdade <= $idadeMeiaIdade) {
    echo 'Sou um adulto de meia idade!<br>';
} else {
    echo 'Sou um idoso!<br>';
}