<?php

$nome = 'Gabriel';

switch ($nome) {
    case 'Luana':
        echo 'Fala Luana!<br>';
        break;
    case 'José':
        echo 'Opa, na verdade é o José! Fala José!<br>';
        break;
    case 'Gabriel':
        echo 'Hahahaha, é o Gabriel! :)';
        break;
    default:
        echo 'Eu não conheço ninguem nessa festa! :(';
}